# Practice git rebase

- Commit 1
- Commit 2
- Commit 3
- Commit 4
- Commit A5
- Commit A6
- Commit B7
- Commit B8
- Commit B9
- Commit B10
- Commit B11

- Commit C12
- Commit C13
- Commit C14
- New commit C1 15
- New commit C1 16
  
- New commit after C3 B1
- New commit after C3 B12

- New commit after C3 B12
- New commit after C3 1
- New commit after C3 A7
- Commit from branch C -> C3 15
  

## How to check problem with git rebase & conflitc

- **Step 1**: Keep going `git rebase --continue`
- **Step 2**: Fix manually `conflict` then `git add .`, ideal fix with vscode
- Back to **Step 1**, now if it says **no changes .......**
- Run `git rebase --skip` and go back to **Step 1** --> `git rebase --continue`
- If you just want to quit rebase run `git rebase --abort`
- Once all changes are done run `git commit -m "rebase complete"` and you are done

Note: If you don't know what's going on and just want to go back to where the repo was, then just do: 

```bash
git rebase --abort

```

## References

- Read about rebase: [git-rebase doc](https://www.atlassian.com/git/tutorials/rewriting-history/git-rebase)

- Check more how to resolve error: `rebase in progress ...` on [stackoverflow](https://stackoverflow.com/questions/29902967/rebase-in-progress-cannot-commit-how-to-proceed-or-stop-abort)

- [Difference between rebase vs merge (vietnamese)](https://lvhan.medium.com/merge-v%C3%A0-rebase-e72236c40368)

- GUI git to check graph of all branches: [GitKraken](https://www.gitkraken.com/)